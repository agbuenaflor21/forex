import React from 'react';
import CalculatorButton from './CalculatorButton';




function CalculatorButtons(props) {
    return (
        <>
            <CalculatorButton

                text={'+'}
                handleOnClick={props.handleAdd}
                color={'btn-success'}
            />
            <CalculatorButton
                text={'-'}
                handleOnClick={props.handleMinus}
                color={'btn-danger'}
            />
            <CalculatorButton
                text={'x'}
                handleOnClick={props.handleMultiply}
                color={'btn-warning'}
            />
            <CalculatorButton
                text={'/'}
                handleOnClick={props.handleDivide}
                color={'btn-primary'}
            />
            <CalculatorButton
                text={'Reset'}
                handleOnClick={props.handleReset}
                color={'btn-secondary'}

            />





        </>
    )
}

export default CalculatorButtons;