import React, { useState } from 'react';
import CalculatorInput from './CalculatorInput';
import CalculatorButtons from './CalculatorButtons';

const Calculator = () => {

    const [input1, setInput1] = useState(0);
    const [input2, setInput2] = useState(0);
    const [count, setCount] = useState(0);
    const [convertedAmount, setConvertedAmount] = useState(0);


    const handleInput1 = e => {

        setInput1(e.target.value);
    }

    const handleInput2 = e => {

        setInput2(e.target.value);
    }

    const handleAdd = () => {


        setConvertedAmount(parseInt(input1) + parseInt(input2));
    }

    const handleMinus = () => {


        setConvertedAmount(parseInt(input1) - parseInt(input2));
    }

    const handleMultiply = () => {


        setConvertedAmount(parseInt(input1) * parseInt(input2));
    }

    const handleDivide = () => {


        setConvertedAmount(parseInt(input1) / parseInt(input2));
    }

    const handleReset = () => {
        setConvertedAmount(0);
    }











    return (
        <>
            <h1>Calculator</h1>
            <div>
                <CalculatorInput
                    label={'Input 1'}
                    placeholder={'Insert Number'}
                    onChange={handleInput1}

                />
            </div>

            <div>
                <CalculatorInput
                    label={'Input 2'}
                    placeholder={'Insert Number'}
                    onChange={handleInput2}
                />
            </div>

            <CalculatorButtons
                handleAdd={handleAdd}
                handleMinus={handleMinus}
                handleMultiply={handleMultiply}
                handleDivide={handleDivide}
                handleReset={handleReset}
            />

            <h1>{convertedAmount}</h1>

        </>
    )
}

export default Calculator;