import React from 'react';
import { Button } from 'reactstrap';


function CalculatorButton(props) {
    return (
        <Button
            className={props.color}
            onClick={props.handleOnClick}

        >{props.text}</Button>

    );
}

export default CalculatorButton;