import React from 'react';
import Calculator from './calcComponents/Calculator';

const CalculatorApp = () => {
    return (
        <div
            className='bg-info d-flex justify-content-center align-items-center min-vh-100 vh-100'
        >
            <Calculator />
        </div>
    )
}

export default CalculatorApp;