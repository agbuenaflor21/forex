import React, { useState } from 'react';
import ForexDropdown from './ForexDropdown';
import ForexInput from './ForexInput';
import ForexRate from './ForexRate';
import ForexTable from './ForexTable';
import { Button } from 'reactstrap';

const Forex = () => {


    // Transform this to functional syntax
    // state = {
    //     amount: 0,
    //     baseCurrency: null,
    //     targetCurrency: null,
    //     convertedAmount: 0,

    // }

    const [amount, setAmount] = useState(0);
    const [baseCurrency, setBaseCurrency] = useState(null);
    const [targetCurrency, setTargetCurrency] = useState(null);
    const [convertedAmount, setConvertedAmount] = useState(0);
    const [targetCode, setTargetCode] = useState(0);
    const [showTable, setShowTable] = useState(false);
    const [rates, setRates] = useState([]);

    const handleAmount = e => {
        // this.setState({
        //     amount: e.target.value
        // });
        setAmount(e.target.value);
    }

    const handleBaseCurrency = currency => {
        // this.setState({
        //     baseCurrency: currency
        // });
        const code = currency.code;

        fetch('https://api.exchangeratesapi.io/latest?base=' + code)
            .then(res => res.json()) // transform to data hat we can read
            .then(res => {
                const ratesArray = Object.entries(res.rates);
                console.log(ratesArray);

                setRates(ratesArray);
                setShowTable(true);
            });



        setBaseCurrency(currency);
    }

    const handleTargetCurrency = currency => {
        // this.setState({
        //     targetCurrency: currency
        // });
        setTargetCurrency(currency);
    }

    const handletargetCode = currency => {
        setTargetCode(currency)
    }


    const handleConvert = () => {   //{ currency: "" code: ""} data from ForexData


        const code = baseCurrency.code;

        fetch('https://api.exchangeratesapi.io/latest?base=' + code)
            .then(res => res.json())
            .then(res => {
                const targetCode = targetCurrency.code;

                const rate = res.rates[targetCode];

                // console.log(rate);
                // this.setState({ convertedAmount: this.state.amount * rate })

                setConvertedAmount(amount * rate);
            });
    }
    //start of activity
    const handleRate = () => {
        const code = this.state.baseCurrency.code;

        fetch('https://api.exchangeratesapi.io/latest?base=' + code)
            .then(res => res.json())
            .then(res => {
                const targetCode = this.state.targetCurrency.code;

                const rate = res.rates[targetCode];




                this.setState({ convertedAmount: this.state.amount * rate })

            });
    }

    //end of activity



    return (
        <div
            style={{ width: "70%" }}
        >
            <h1 className='text-center my-5'>Forex Calculator</h1>
            {
                showTable === true
                    ?
                    <div>
                        <h2 className='text-center'>Exchange Rate for:{baseCurrency.currency}</h2>
                        <div className='d-flex justify-content-center'>

                            <ForexTable
                                rates={rates}
                            />
                        </div>

                    </div>
                    : ""
            }
            <div
                className='d-flex justify-content-around'
                style={{ margin: '0 200px' }}
            >
                <ForexDropdown
                    label={'Base Currency'}
                    onClick={handleBaseCurrency}
                    currency={baseCurrency}
                />
                <ForexDropdown
                    label={'Target Currency'}
                    onClick={handleTargetCurrency}
                    currency={targetCurrency}
                />

            </div>
            <div
                className='d-flex justify-content-around'
            >
                <ForexInput
                    label={'Amount'}
                    placeholder={'Amount to convert'}
                    onChange={handleAmount}
                />
                <Button
                    color='info'
                    onClick={handleConvert}
                >
                    Convert
                    </Button>
            </div>
            <div>
                <h1 className='text-center'>{
                    // start of activity
                    // this.handleAmount < 1 ||
                    //     this.handleAmount == null ||
                    //     this.state.baseCurrency == null ||
                    //     this.state.targetCurrency == null
                    //     ? "Fill out everything to get the result"
                    //     : 
                    convertedAmount
                    // end of activity

                }</h1>

            </div>
            <ForexRate

                onClick={handleRate}
                allResult={convertedAmount}

            />



        </div >
    )

}

export default Forex;