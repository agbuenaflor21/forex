// see note in the import section of ForexInput for the transition of classbased to functional
// useState is being imported to define the state
import React, { useState } from 'react';
import { FormGroup, Label, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import Currencies from './ForexData';


function ForexDropdown(props) {

    // state is only used for class-based components, so we're going to transform it to functional via useState to define the state (so below is commented-out)
    // state = {
    //   dropdownIsOpen: false,
    //   currency: null
    // }

    // In useState, the first one inside the array is the name of state, then the second is the one who will modify the state
    const [dropdownIsOpen, setDropdownIsOpen] = useState(false);
    const [currency, setCurrency] = useState(null);


    // Again, render is a class-based..
    // render() {

    // console.log(Currencies);

    return (
        <FormGroup>
            <Label>{props.label}</Label>
            <Dropdown
                // classbased:
                // isOpen={this.state.dropdownIsOpen}

                // functional based:
                isOpen={dropdownIsOpen}

                // classbased:
                // toggle={() => this.setState({ dropdownIsOpen: !this.state.dropdownIsOpen })}

                // functional based:
                toggle={() => setDropdownIsOpen(!dropdownIsOpen)}
            >
                <DropdownToggle caret>{!props.currency ? "Choose Currency" : props.currency.currency}</DropdownToggle>
                <DropdownMenu>

                    {/* map is equivalent of foreach loop then index is the count or pangilan ung data na knkuha mo */}
                    {Currencies.map((currency, index) => (
                        <DropdownItem
                            key={index}
                            onClick={() => props.onClick(currency)}
                        >
                            {currency.currency}
                        </DropdownItem>
                    ))}
                </DropdownMenu>
            </Dropdown>
        </FormGroup>
    );
    // }
}

export default ForexDropdown;