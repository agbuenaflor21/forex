import React, { Component } from 'react';
import { FormGroup, Dropdown, Label, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import Currencies from './ForexData'


class ForexRate extends Component {

    state = {
        dropdownIsOpen: false,
        currency: {},
        convertedAmount: {}
    }

    render() {
        return (
            // <h1>ForexRate</h1>
            <FormGroup>
                <Label></Label>
                <Dropdown
                    isOpen={this.state.dropdownIsOpen}
                    toggle={() => this.setState({ dropdownIsOpen: !this.state.dropdownIsOpen })}
                >
                    <DropdownToggle caret>Rate

                    </DropdownToggle>
                    <DropdownMenu>
                        {Currencies.map((currency, index) => (
                            <DropdownItem
                                key={index}
                                onClick={() => this.props.onClick(currency)}
                            >

                                {currency.code + ' : ' + (2 * this.props.allResult)}
                            </DropdownItem>
                        ))}
                    </DropdownMenu>


                </Dropdown>
            </FormGroup>
        )
    }
}

export default ForexRate;