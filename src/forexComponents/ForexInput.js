// class-based:
// import React, {Component} from 'react';

// functional based:
import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';

// classbased:
// class ForexInput extends Component {

// functional based:
function ForexInput(props) {
    // render is classbased
    // render() {

    // functional holds return
    return (
        <FormGroup>
            <Label>{props.label}</Label>
            <Input
                // we will remove this inside the object, since classbased
                placeholder={props.placeholder}
                defaultValue={props.defaultValue}
                onChange={props.onChange}
                type='number'
            />
        </FormGroup>
    );
}
// }

export default ForexInput;